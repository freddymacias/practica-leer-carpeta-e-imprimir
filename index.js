import { createRequire } from "module";
const require = createRequire(import.meta.url);

import chalk from 'chalk';
import _ from "pdf-to-printer";
const { print } = _;

const chokidar = require('chokidar');
const path = require('path');

const watchPath = path.join('C:/Users/HP/Desktop/practica/renueva/document', '*.pdf');
const watchOptions = {
  persistent   : true,
  ignoreInitial: true
}

const watcher = chokidar.watch(watchPath, watchOptions);
console.log(chalk.blue(`Observando ${watchPath}...`));

watcher.on('add', path => {

  console.log(chalk.blue(`Nuevo archivo encontrado: ${path}`));
  console.log(chalk.yellow('Imprimiendo...'));

  const printOptions = [
    { printer: "EPSON L365 Series" },
    //{ printer: "Multifuncional 123" },
  ];

  let printerTasks = printOptions.map((options, i) => {
    return print(path, options)
      .then(() => {
        console.log(chalk.green(`Archivo [${path}] impreso en impresora ${i + 1} (${options.printer})`));
      })
      .catch((error) => {
        console.log(chalk.red(`Error al imprimir en impresora ${i + 1} (${options.printer})`))
        console.log(chalk.red(error));
      });
  });

  Promise.all(printerTasks).finally(() => {
    console.log(chalk.green(`Observando ${watchPath}...`));
  });

})